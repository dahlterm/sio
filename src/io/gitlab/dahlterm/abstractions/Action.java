package io.gitlab.dahlterm.abstractions;

import java.util.UUID;

//an object capability action
public interface Action {
	public String actionTitle();
	public void performAction(ObjectCapabilities target);
	public String helpInformation();
	public UUID identifier();
}
