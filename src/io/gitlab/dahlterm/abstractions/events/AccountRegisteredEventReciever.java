package io.gitlab.dahlterm.abstractions.events;

import io.gitlab.dahlterm.abstractions.protocol.Account;

public interface AccountRegisteredEventReciever {
	public void accountRegistered(Account acc);
}
