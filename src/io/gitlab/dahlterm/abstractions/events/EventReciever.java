package io.gitlab.dahlterm.abstractions.events;

public interface EventReciever {
	public void receiveEvent(Object event);
}
