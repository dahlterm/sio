package io.gitlab.dahlterm.abstractions.protocol;

import java.awt.Component;

import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;
import io.gitlab.dahlterm.abstractions.events.EventReciever;

public interface Message extends ObjectCapabilities {
	public String asText();
	public Component customComponent();
	public LayoutNode container();
	public void setUpdateReciever(EventReciever er);
}
