package io.gitlab.dahlterm.implementation.interfacing;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import io.gitlab.dahlterm.abstractions.Action;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;

public class GenericSelectionList extends VerticalScrollOnlyPanel {
	
	public Action selected = null;
	
	public ObjectCapabilities wrapped = null;
	
	public GenericSelectionList(ObjectCapabilities wrapTarget) {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		wrapped = wrapTarget;
		updateComponents();
	}
	
	@Override
	public Insets getInsets() {
		return new Insets(5, 5, 5, 5);
	}
	
	public void updateComponents() {
		ButtonGroup buttonsList = new ButtonGroup();
		this.removeAll();
		if(wrapped == null) {
			System.out.println("Strange! Generic selection list " + this.getName() + " lacks a list to display!");
			return;
					
		}
		if(wrapped.properties() == null) {
			System.out.println("Strange! Generic selection list " + this.getName() + " has an empty list!");
			return;
		}
		for(Action button : wrapped.properties()) {

			final Action f_button = button;
			JToggleButton selectionButton = new JToggleButton("<html>" + button.actionTitle() + "</html>");
			selectionButton.setToolTipText(button.helpInformation());
			selectionButton.setHorizontalAlignment(SwingConstants.LEFT);
			buttonsList.add(selectionButton);
			this.add(selectionButton);
			
			if(selected != null) {
				if(selected.identifier().equals(button.identifier())) {
					selectionButton.setSelected(true);
				}
			}
			
			selectionButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selected = f_button;
					f_button.performAction(wrapped);
				}
			});
		}
	}
}
