package io.gitlab.dahlterm.implementation.interfacing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import io.gitlab.dahlterm.abstractions.Action;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;

public class PopupMenus {
	public static JPopupMenu createMenu(ObjectCapabilities target, String localization) {
		// final versions let us access these in anon classes
		final ObjectCapabilities ocapTarget = target;
		final JPopupMenu result = new JPopupMenu();
		
		Action[] elements = target.properties();
		for(Action item : elements) {
			final Action finalItem = item;
			JMenuItem menuItem = new JMenuItem(item.actionTitle());
			menuItem.setToolTipText(item.helpInformation());
			menuItem.addActionListener(new ActionListener() {
				Action menuAction = finalItem;
				ObjectCapabilities capabilitiesTarget = ocapTarget;
				public void actionPerformed(ActionEvent e) {
					menuAction.performAction(capabilitiesTarget);
				}
				
			});
			result.add(menuItem);
		}
		return result;
	}
}
