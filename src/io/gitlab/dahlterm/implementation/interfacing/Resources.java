package io.gitlab.dahlterm.implementation.interfacing;

import java.awt.Image;
import java.net.URL;

import javax.imageio.ImageIO;

import io.gitlab.dahlterm.implementation.EntryPoint;

public class Resources {
	public static Image loadResource(String resourceIdentifier) {
		URL url = EntryPoint.class.getResource(resourceIdentifier);
		if(url == null) return null;
		try {
			return ImageIO.read(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
