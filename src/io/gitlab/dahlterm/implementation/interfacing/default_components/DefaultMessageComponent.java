package io.gitlab.dahlterm.implementation.interfacing.default_components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import io.gitlab.dahlterm.abstractions.protocol.Message;
import io.gitlab.dahlterm.implementation.interfacing.PopupMenus;

public class DefaultMessageComponent extends JPanel {
	
	JButton actionMenu = new JButton("...");
	JTextArea messageText = new JTextArea();
	Message message = null;
	Component asComponent = (Component)this;

	public DefaultMessageComponent(Message msg) {
		this.setBackground(null);
		this.setBorder(BorderFactory.createEmptyBorder());
		message = msg;
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		if(msg != null) {
			messageText.setText(msg.asText());
		}
		messageText.setBackground(null);
		messageText.setBorder(BorderFactory.createEmptyBorder());
		messageText.setEditable(false);
		messageText.setLineWrap(true);
		messageText.setWrapStyleWord(true);
		messageText.setMinimumSize(new Dimension(10, 10));
		messageText.setAlignmentX(Component.LEFT_ALIGNMENT);
		actionMenu.setAlignmentX(Component.RIGHT_ALIGNMENT);
		messageText.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(messageText);
		add(actionMenu);
		actionMenu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(message == null) return;
				if(message.properties() != null) {
					JPopupMenu menu = PopupMenus.createMenu(message, "en-us");
					menu.show(asComponent, actionMenu.getX(), asComponent.getHeight());
				}
			}
			
		});
	}
}
