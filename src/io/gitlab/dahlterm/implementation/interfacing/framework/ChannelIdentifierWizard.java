package io.gitlab.dahlterm.implementation.interfacing.framework;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JWindow;
import java.util.*;

import io.gitlab.dahlterm.abstractions.Action;
import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;
import io.gitlab.dahlterm.abstractions.events.EventReciever;
import io.gitlab.dahlterm.abstractions.protocol.Account;
import io.gitlab.dahlterm.abstractions.protocol.Channel;
import io.gitlab.dahlterm.abstractions.protocol.Protocol;
import io.gitlab.dahlterm.implementation.PersistentData;
import io.gitlab.dahlterm.implementation.interfacing.GenericSelectionList;

public class ChannelIdentifierWizard extends JFrame {

	ObjectCapabilities accountsCapabilitiesSelectorConverter(final ArrayList<Account> accounts) {
		final ChannelIdentifierWizard parent = this;
		return new ObjectCapabilities() {
			public Action[] properties() {

				ArrayList<Action> res = new ArrayList<Action>();

				for (Account a : accounts) {
					final Account selector = a;
					res.add(new Action() {

						public void performAction(ObjectCapabilities target) {
							parent.selectedAccount = selector;
							parent.updateAllLists();
						}

						public UUID identifier() {
							return selector.identifier();
						}

						public String helpInformation() {
							return selector.nickname();
						}

						public String actionTitle() {
							return selector.nickname();
						}
					});
				}
				if (res.isEmpty())
					return null;

				return res.toArray(new Action[1]);
			}
		};
	}

	ObjectCapabilities protocolsCapabilitiesSelectorConverter(final ArrayList<Protocol> protocols) {
		final ChannelIdentifierWizard parent = this;
		return new ObjectCapabilities() {
			public Action[] properties() {

				ArrayList<Action> res = new ArrayList<Action>();

				for (Protocol p : protocols) {
					final Protocol selector = p;
					res.add(new Action() {

						public void performAction(ObjectCapabilities target) {
							parent.selectedProtocol = selector;
							parent.updateAllLists();
						}

						public UUID identifier() {
							return selector.identifier();
						}

						public String helpInformation() {
							return selector.nickname();
						}

						public String actionTitle() {
							return selector.nickname();
						}
					});
				}
				if (res.isEmpty())
					return null;

				return res.toArray(new Action[1]);
			}
		};
	}

	ObjectCapabilities channelsCapabilitiesSelectorConverter(final ArrayList<Channel> channels) {
		final ChannelIdentifierWizard parent = this;
		return new ObjectCapabilities() {
			public Action[] properties() {

				ArrayList<Action> res = new ArrayList<Action>();

				for (Channel c : channels) {
					final Channel selector = c;
					res.add(new Action() {

						public void performAction(ObjectCapabilities target) {
							parent.selectedChannel = selector;
							parent.updateAllLists();
							parent.textResulting.setText(selector.owningAccount().owningProtocol().nickname() + ":"
									+ selector.owningAccount().nickname() + ":" + selector.channelIdentifier());
						}

						public UUID identifier() {
							return selector.identifier();
						}

						public String helpInformation() {
							return selector.title();
						}

						public String actionTitle() {
							return selector.title();
						}
					});
				}
				if (res.isEmpty())
					return null;

				return res.toArray(new Action[1]);
			}
		};
	}

	JTextField textResulting = new JTextField();

	GenericSelectionList protocolList, accountList, channelList;

	Protocol selectedProtocol = null;
	Account selectedAccount = null;
	Channel selectedChannel = null;

	EventReciever returnPath = null;
	LayoutNode targetNode = null;

	public void setupDialogTopLevelInterface() {
		//this.removeAll();

		final ChannelIdentifierWizard self = this;
		setTitle("Channel Selection Wizard");

		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);

		GridBagConstraints tabPanelConstraints = new GridBagConstraints();
		JTabbedPane selectionMethod = new JTabbedPane();
		selectionMethod.setTabPlacement(JTabbedPane.BOTTOM);
		tabPanelConstraints.fill = GridBagConstraints.BOTH;
		tabPanelConstraints.gridx = 0;
		tabPanelConstraints.gridy = 0;
		tabPanelConstraints.gridwidth = 4;
		tabPanelConstraints.weighty = 1.0;
		tabPanelConstraints.weightx = 1.0;
		tabPanelConstraints.insets = new Insets(5, 5, 5, 5);
		JPanel treeSelection = new JPanel();
		selectionMethod.addTab("Tree Selector", treeSelection);

		JPanel sequentialSelection = new JPanel();
		selectionMethod.addTab("Sequential Selector", sequentialSelection);

		add(selectionMethod, tabPanelConstraints);

		textResulting.setToolTipText(
				"This will contain the valid channel identifier you've selected. It is composed of: protocol:account:channel codes, in that order.");
		textResulting.setEditable(false);
		GridBagConstraints resultingConstraints = new GridBagConstraints();
		resultingConstraints.fill = GridBagConstraints.HORIZONTAL;
		resultingConstraints.gridheight = 1;
		resultingConstraints.gridwidth = 4;
		resultingConstraints.gridx = 0;
		resultingConstraints.gridy = 1;
		resultingConstraints.insets = new Insets(5, 5, 5, 5);
		add(textResulting, resultingConstraints);

		GridBagConstraints buttonPanelConstraints = new GridBagConstraints();
		buttonPanelConstraints.fill = GridBagConstraints.NONE;
		buttonPanelConstraints.gridx = 2;
		buttonPanelConstraints.gridy = 3;
		buttonPanelConstraints.gridwidth = 2;
		buttonPanelConstraints.anchor = GridBagConstraints.EAST;
		buttonPanelConstraints.insets = new Insets(5, 5, 5, 5);

		JPanel buttonPanel = new JPanel();
		GridBagLayout buttonPanelLayout = new GridBagLayout();
		buttonPanel.setLayout(buttonPanelLayout);

		GridBagConstraints cancelButtonConstraints = new GridBagConstraints();
		cancelButtonConstraints.fill = GridBagConstraints.NONE;
		cancelButtonConstraints.gridx = 0;
		cancelButtonConstraints.anchor = GridBagConstraints.EAST;
		cancelButtonConstraints.insets = new Insets(5, 5, 5, 5);
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				self.setVisible(false);
			}
		});
		buttonPanel.add(cancelButton, cancelButtonConstraints);

		GridBagConstraints resetButtonConstraints = new GridBagConstraints();
		resetButtonConstraints.fill = GridBagConstraints.NONE;
		resetButtonConstraints.gridx = 1;
		resetButtonConstraints.anchor = GridBagConstraints.EAST;
		resetButtonConstraints.insets = new Insets(5, 5, 5, 5);
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				setupDialogTopLevelInterface();
			}
		});
		buttonPanel.add(resetButton, resetButtonConstraints);

		GridBagConstraints applyButtonConstraints = new GridBagConstraints();
		applyButtonConstraints.fill = GridBagConstraints.NONE;
		applyButtonConstraints.gridx = 2;
		applyButtonConstraints.anchor = GridBagConstraints.EAST;
		applyButtonConstraints.insets = new Insets(5, 5, 5, 5);
		JButton applyButton = new JButton("Finish");
		applyButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				targetNode.channelIdentifier = textResulting.getText();
				returnPath.receiveEvent(textResulting.getText());
				self.setVisible(false);
			}
		});
		buttonPanel.add(applyButton, applyButtonConstraints);

		add(buttonPanel, buttonPanelConstraints);

		setupSequentialSelectionTab(sequentialSelection);

		pack();
	}

	JComponent wrapWithTextPanel(JComponent contained, String text) {
		JPanel container = new JPanel();
		container.setBorder(BorderFactory.createTitledBorder(text));
		container.add(contained);
		return container;
	}

	void setupSequentialSelectionTab(JPanel container) {
		container.setLayout(new GridLayout(1, 3));

		protocolList = new GenericSelectionList(
				protocolsCapabilitiesSelectorConverter(PersistentData.getUserProfile().getRegisteredProtocols()));
		container.add(wrapWithTextPanel(protocolList, "Narrow Down By Protocol"));

		accountList = new GenericSelectionList(accountsCapabilitiesSelectorConverter(PersistentData.accountsBy(null)));
		container.add(wrapWithTextPanel(accountList, "Narrow Down By Account"));

		channelList = new GenericSelectionList(channelsCapabilitiesSelectorConverter(PersistentData.channelsBy(null)));
		container.add(wrapWithTextPanel(channelList, "Select Channel"));

	}

	public void updateAllLists() {
	}

	public static void show(LayoutNode target, EventReciever update) {
		ChannelIdentifierWizard window = new ChannelIdentifierWizard();
		window.returnPath = update;
		window.targetNode = target;
		window.setupDialogTopLevelInterface();
		window.setVisible(true);
	}
}
