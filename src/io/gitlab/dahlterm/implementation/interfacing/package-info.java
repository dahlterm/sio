/*
 * implemented.interfacing is a package that contains user interface code.
 * The name seems vague, and could be considered confusing, compared to other
 * options to name it, but as a rule I prefer single-word package names, and
 * this is sufficient for that purpose. you may notice this is *underneath*
 * the implemented package, and that is to encourage abstractions to stick to
 * swing abstractions for components, such as JComponent, for a generic or
 * abstract version.
 */
package io.gitlab.dahlterm.implementation.interfacing;